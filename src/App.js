import './App.css';
import Gridbox from './include/gridbox/Gridbox'
import Flexbox from './flexbox/Flexbox'

const App = () => {
  return (
    <div className='App Font'>
      <strong>GridBox</strong>
      <Gridbox />

      <strong>Flexbox</strong>
      <Flexbox/>
    </div>
  )
}

export default App