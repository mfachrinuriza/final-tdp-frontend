import './Gridbox.css'
import Main from '../gridbox/efg/Main'


const Gridbox = () => {
    return(
        <div class="grid-container">
            <div class="item1">A</div>
            {/* <div class="item2"></div> */}
            <div class="item3">B</div>  
            <div class="item4">C</div>
            <div class="item5">
                <Main />
            </div>
        </div>
    )
}

export default Gridbox