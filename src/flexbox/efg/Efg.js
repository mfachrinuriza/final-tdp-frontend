import './Efg.css'

const Efg = () => {
    return(
        <div>
            <div className='main'>
                <div class="flex-container-main">
                    <div class="left">E</div>
                </div>
                <div className="flex-container-main">
                    <div class="right">F</div>
                    <div class="footer">G</div>
                </div>
            </div>
        </div>
    )
}

export default Efg