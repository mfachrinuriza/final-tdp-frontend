import './Flexbox.css'
import Efg from './efg/Efg'
const Flexbox = () => {
    return(
        <div className='flexbox-container'>
            <div className='atas'>
                <div className='box a'>A</div>
                <div className='box b'>B</div>
            </div>  
            <div className='bawah'>
                <div className='box c'>C</div>
                <div className='box d'>
                    <Efg/>
                </div>
            </div>      
      </div>
    )
}

export default Flexbox